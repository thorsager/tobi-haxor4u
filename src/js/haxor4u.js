let SOURCE="";
let OFFSET=0;
let MAX_SPEEDUP=10;
let HAX_URL='resources/gibberish.txt';
let WHEN=Math.floor(Math.random() * 500) + 200;
let WHEN_COUNT=0;

function loadHax() {
    let request = new XMLHttpRequest();
    request.open('GET',HAX_URL,true);
    request.send(null);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            SOURCE=request.response
        }
    };
}

function dialogKeyStroke(e) {
    console.log(e);
    e.stopPropagation();
    if (e.code==="Enter") {
        if (document.getElementById("x-user").value === "neo" && document.getElementById('x-password').value === "matrix") {
            document.getElementById('dialog').innerHTML="<h1>Access Granted!</h1>"
            document.removeEventListener("keypress",addKeyStroke);
        } else {
            hideDialog();
        }
    }
    return false;
}

function showDialog() {
    let dialog = document.getElementById('dialog');
    let xuser=document.getElementById('x-user');
    xuser.value="" ;
    document.getElementById('x-password').value='';
    dialog.style.visibility="visible";
    xuser.focus();
    document.getElementById('x-user').focus();
    dialog.addEventListener("keypress", dialogKeyStroke);
}

function hideDialog() {
    let dialog = document.getElementById('dialog');
    dialog.style.visibility="hidden";
    dialog.removeEventListener("keypress", dialogKeyStroke);
}

function addKeyStroke(e) {
    let view = document.getElementById('haxor_view');

    if (WHEN_COUNT >= WHEN) {
        showDialog();
        WHEN=Math.floor(Math.random() * 500) + 200;
        WHEN_COUNT=0;
    }

    if (OFFSET===0) {
        view.innerHTML='';
    }

    let speedup = Math.floor(Math.random() * MAX_SPEEDUP) + 1;
    for (let i=0; i<speedup; i++) {
        view.innerHTML += SOURCE.charAt(OFFSET++ % SOURCE.length);
        window.scrollTo(0,document.body.scrollHeight);
    }
    WHEN_COUNT+=speedup;
}

document.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");

    loadHax();

    document.addEventListener("keypress", addKeyStroke);
});